package ciber.it.academy.component;

import java.util.Date;

public class CurrentDateProvider {

    public void initMethod() {
        System.out.println("bean is created " + this.getCurrentDate());
    }

    public Date getCurrentDate() {
        return new Date();
    }

}
