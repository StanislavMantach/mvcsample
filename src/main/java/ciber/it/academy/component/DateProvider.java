package ciber.it.academy.component;

import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.PostConstruct;
import java.util.Date;

public class DateProvider {

    @Autowired
    private CurrentDateProvider currentDateProvider;

    @PostConstruct
    public void initMethod() {
        System.out.println("provided date on post construct " + this.provideDate());
    }

    public Date provideDate() {
        return currentDateProvider.getCurrentDate();
    }

}
