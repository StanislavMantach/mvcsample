package ciber.it.academy.mvc.controller;

import ciber.it.academy.component.DateProvider;
import ciber.it.academy.service.AsyncLoggingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class HelloController {

    @Value("${properties.value}")
    private String propValue;

    @Autowired private DateProvider dateProvider;
    @Autowired private AsyncLoggingService asyncLoggingService;

    @RequestMapping(value = "/hello", method = RequestMethod.GET)
    public String hello(Model model) {
        model.addAttribute("variable", "World");
        model.addAttribute("date", dateProvider.provideDate());
        model.addAttribute("value", propValue);
        asyncLoggingService.writeLogAsynchroniously();
        System.out.println("date in controller " + dateProvider.provideDate() + " " + Thread.currentThread());
        return "hello";
    }

    @RequestMapping(value = "/path/{path}")
    public String showPathVariable(@PathVariable String path, Model model) {
        model.addAttribute("pathVariable", path);
        return "variables";
    }

    @RequestMapping(value = "/path")
    public String showRequestParam(@RequestParam(required = false, defaultValue = "notProvided") String path, Model model) {
        model.addAttribute("pathVariable", path);
        return "variables";
    }

}
