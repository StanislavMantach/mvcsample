package ciber.it.academy.mvc.controller;

import ciber.it.academy.service.Person;
import ciber.it.academy.service.PersonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class PersonController {

    @Autowired
    private PersonService personService;

    @RequestMapping(value = "/person")
    public String showPerson(Model model) {
        model.addAttribute("person", personService.getPerson());
        return "person";
    }

    @RequestMapping(value = "/person", method = RequestMethod.POST)
    public String updatePerson(@ModelAttribute Person person, Model model) {
        personService.updatePerson(person);
        model.addAttribute("person", personService.getPerson());
        return "person";
    }

}
