package ciber.it.academy.service;

import ciber.it.academy.component.DateProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

@Service
public class AsyncLoggingService {

    @Autowired private DateProvider dateProvider;

    @Async
    public void writeLogAsynchroniously(){
        try {
            Thread.sleep(200);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("async date logging " + dateProvider.provideDate() + " " + Thread.currentThread());
    }

}
