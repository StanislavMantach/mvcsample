package ciber.it.academy.service;

import org.springframework.stereotype.Service;

@Service
public class PersonService {

    private static Person person = new Person("John", "Smith");

    public Person getPerson() {
        return PersonService.person;
    }

    public void updatePerson(Person person) {
        PersonService.person = person;
    }
}
