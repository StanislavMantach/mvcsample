<html>
<head>
    <title>Hello Spring MVC</title>
    <link href="/resources/css/bootstrap.min.css" rel="stylesheet">
</head>
<body>
<div class="container">
    <h2>Hello ${variable}!</h2>
    <h2>${date}!</h2>
    <h2>${value}!</h2>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script src="/resources/js/bootstrap.min.js"></script>
</body>
</html>
