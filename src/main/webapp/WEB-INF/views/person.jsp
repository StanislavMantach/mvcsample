<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<html>
<head>
    <title>Hello Spring MVC</title>
    <link href="/resources/css/bootstrap.min.css" rel="stylesheet">
</head>
<body>
<div class="container">
    <h3>Person Details Page</h3>
    <form:form method="POST" modelAttribute="person">

        <div class="form-group">
            <form:label path="name">Name</form:label>
            <form:input path="name"/>
        </div>
        <div class="form-group">
            <form:label path="surname">surname</form:label>
            <form:input path="surname"/>
        </div>

        <button type="submit" class="btn btn-default">Submit</button>

    </form:form>

</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script src="/resources/js/bootstrap.min.js"></script>
</body>
</html>