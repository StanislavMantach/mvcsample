<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>

<html>
<head>
    <title>Hello Spring MVC</title>
    <link href="/resources/css/bootstrap.min.css" rel="stylesheet">
</head>
<body>
<div class="container">
    <c:choose>
        <c:when test="${pathVariable == 'notProvided'}">
            <h2>this variable is not provided</h2>
        </c:when>
        <c:otherwise>
            <h2>Path variable ${pathVariable}!</h2>
        </c:otherwise>
    </c:choose>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script src="/resources/js/bootstrap.min.js"></script>
</body>
</html>
